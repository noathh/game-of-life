class Empty:
    def __init__(self, x, y):
        self.x, self.y = x, y
        self.cell_type = 'n'
        self.fish_to_push = 0
        self.shrimp_to_push = 0

    # reproducing is standard push for empty cells
    def push(self, table, new_table):
        self.fish_to_push, self.shrimp_to_push = self.update_nearby(table)

        # if you want to add new type of unit, add its reproducing method here
        if self.fish_to_push == 3:
            new_table[self.x][self.y] = Fish(self.x, self.y)
        elif self.shrimp_to_push == 3:
            new_table[self.x][self.y] = Shrimp(self.x, self.y)
        else:
            new_table[self.x][self.y] = table[self.x][self.y]

    def update_nearby(self, table):
        n = len(table)
        m = len(table[0])
        fish = 0
        shrimp = 0

        for x in range(self.x - 1, self.x + 2):
            for y in range(self.y - 1, self.y + 2):
                if x == self.x and y == self.y:
                    continue
                elif n > x > 0 and m > y > 0:
                    if type(table[x][y]) == Fish:
                        fish += 1
                    if type(table[x][y]) == Shrimp:
                        shrimp += 1

        return fish, shrimp


class Rock(Empty):
    def __init__(self, x, y):
        super(Rock, self).__init__(x, y)
        self.cell_type = 'r'

    def push(self, table, new_table):
        new_table[self.x][self.y] = Rock(self.x, self.y)


class Fish(Empty):
    def __init__(self, x, y):
        super(Fish, self).__init__(x, y)
        self.cell_type = 'f'

    def push(self, table, new_table):
        fish = self.update_nearby(table)[0]

        if fish >= 4 or fish < 2:
            new_table[self.x][self.y] = Empty(self.x, self.y)
        else:
            new_table[self.x][self.y] = Fish(self.x, self.y)


class Shrimp(Empty):
    def __init__(self, x, y):
        super(Shrimp, self).__init__(x, y)
        self.cell_type = 's'

    def push(self, table, new_table):
        shrimp = self.update_nearby(table)[1]

        if shrimp >= 4 or shrimp < 2:
            new_table[self.x][self.y] = Empty(self.x, self.y)
        else:
            new_table[self.x][self.y] = Shrimp(self.x, self.y)
