import game


def main():
    input_stream, output_stream = game.read_console_params()
    real_n, real_m, real_turns = game.read_table_params(input_stream)
    ocean = game.read_table(real_n, real_m, input_stream)
    ocean = game.run_game(real_n, real_m, real_turns, ocean)
    game.print_table(real_n, real_m, ocean)


if __name__ == '__main__':
    main()
