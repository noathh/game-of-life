# Review-task по курсу Python - Game of life #
##### Идея алгоритма #####
Поддерживаем актуальное состояние игрового поля в table следующим образом: 
идём по клеткам next_table, обновляя информацию клетки next_table[i][j] как 
результат того, что должно быть в клетке table[i][j]. Таким образом, мы заполняем
клетки next_table независимо друг от друга (поскольку они заполняются только на
основе информации из table). Обойдя все клетки, мы получим следующее состояние 
table, записанное в new_table. Очистим table (проинициализируем экземплярами Empty),
поменяем местами new_table и table. Теперь в table находится актуальное состояние
игрового поля после сделанного атомарного расчёта, а new_table - пустое поле. 
Далее итерируемся по ходам (их количеству).

##### Модуль cell.py #####
В модуле cell.py находится реализация объектов, которые могут находиться в клетке.
Все они наследуются от класса Empty и имеют перегруженный метод push(), записывающий
в соответствующую клетку в новой таблице на основе информации о типе этого и соседних 
к нему объектов. Именно поэтому для добавления нового класса со своими правилами
выживания достаточно добавить его в cell.py, унаследовав от Empty для получения
общего интерфейса, и перегрузив внутри него метод push(), который и будет являться
реализацией правила выживания, а также обновить метод read_table() и метод push() 
для Empty (это и есть правило зарождения новой сущности). Также опционально можно
перегрузить метод update_nearby() внутри нового класса, если это необходимо для 
его особенного правила выживания.

##### Модуль game.py #####
В модуле game.py находится реализация функций ввода аргументных параметров, 
ввода-вывода данных и непосредственна функция, запускаящая процесс перезаписывания
информации "table->next_table->table" turns раз.

##### Скрипт main.py #####
Запускает консольный интерфейс, отвечающий на запросы пользователя.

##### Скрипт testcell.py #####
Скрипт testcell.py содержит тестирование методов Empty и его наследников на основе 
файлов в директории cell_input

##### Скрипт testgame.py #####
Проводит интегрированное тестирование на основе входных данных из директории 
input, сравнивая выходные данные с файлами из директории output