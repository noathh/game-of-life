import argparse
import unittest
import os
import sys
import game

unit_parser = argparse.ArgumentParser()
unit_parser.add_argument('-id', '--input-dir', dest='in_dir', help='run unit tests from INPUT_DIR', required=True)
args = unit_parser.parse_args(sys.argv[1:])
test_path = args.in_dir
test_path_in = test_path + "\\input\\"
test_path_out = test_path + "\\output\\"


class TestsForGame(unittest.TestCase):
    def test_game(self):
        print('\n')
        for file in os.listdir(path=test_path_in):
            input_stream = open(test_path_in + file, 'r')
            self.real_n, self.real_m, self.real_turns = game.read_table_params(input_stream)
            self.ocean = game.read_table(self.real_n, self.real_m, input_stream)
            input_stream.close()
            input_stream = open(test_path_out + "output" + file[-6:], 'r')
            answer_table = game.run_game(self.real_n, self.real_m, self.real_turns, self.ocean)
            sample_table = game.read_table(self.real_n, self.real_m, input_stream)

            for i in range(1, self.real_n + 1):
                for j in range(1, self.real_m + 1):
                    self.assertEqual(answer_table[i][j].cell_type, sample_table[i][j].cell_type, file +
                                     " - MISMATCH in cell [{}][{}]".format(i, j))
            print(file, "- OK!")
            input_stream.close()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestsForGame)
    unittest.TextTestRunner(verbosity=2).run(suite)
