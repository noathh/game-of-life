import cell
import sys
import argparse
import numpy


parser = argparse.ArgumentParser()
parser.add_argument('-if', '--input-file', dest='in_file', help='override input stream to file with path IN_FILE')
parser.add_argument('-of', '--output-file', dest='out_file', help='override output stream to file with path OUT_FILE')


def read_console_params():
    print("Type arguments and tap enter. If there are not any arguments tap enter with empty string")
    print("Use -h or --help to get list of arguments")
    args = parser.parse_args(input().split())

    if args.in_file is not None:
        input_stream = open(args.in_file, 'r')
    else:
        input_stream = sys.stdin

    if args.out_file is not None:
        output_stream = open(args.out_file, 'w')
    else:
        output_stream = sys.stdout

    return input_stream, output_stream


def read_table_params(input_stream):
    n, m, turns = (int(a) for a in input_stream.readline().split())
    return n, m, turns


def read_table(n, m, input_stream):

    # ввод игрового поля
    table = list()
    for i in range(n + 2):
        table.append(list())

        if i == 0 or i == n + 1:
            for j in range(m + 2):
                table[i].append(cell.Empty(i, j))
        else:
            input_string = input_stream.readline()
            for j in range(m + 2):
                if j == 0 or j == m + 1:
                    table[i].append(cell.Empty(i, j))
                else:
                    cur_cell = cell.Empty(i, j)
                    cell_type = input_string[j - 1]

                    if cell_type == 'f':
                        cur_cell = cell.Fish(i, j)
                    elif cell_type == 's':
                        cur_cell = cell.Shrimp(i, j)
                    elif cell_type == 'r':
                        cur_cell = cell.Rock(i, j)

                    table[i].append(cur_cell)
    return table


def run_game(n, m, turns, table):
    # расчёт нового игрового поля будет сохраняться в объекте new_table
    next_table = [[cell.Empty(i, j) for j in range(m + 2)] for i in range(n + 2)]

    for cur_turn in range(turns):
        for i in range(1, n + 1):
            for j in range(1, m + 1):
                table[i][j].push(table, next_table)
        table = [[cell.Empty(i, j) for j in range(m + 2)] for i in range(n + 2)]
        table, next_table = next_table, table

    return table


def print_table(n, m, table):
    for i in range(1, n + 1):
        for j in range(1, m + 1):
            print(table[i][j].cell_type, end='')
        print('\n', end='')
