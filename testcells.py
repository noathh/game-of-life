import argparse
import os
import unittest
import cell
import sys
import game

unit_parser = argparse.ArgumentParser()
unit_parser.add_argument('-id', '--input-dir', dest='in_dir', help='run unit tests from INPUT_DIR', required=True)
args = unit_parser.parse_args(sys.argv[1:])

cell_input_path = args.in_dir + "\\cell_input"

# every TestCase is a union of input files
# if one of them will not pass checking


class TestsForCells(unittest.TestCase):
    # test with template: 3x3 for test middle cell
    def test_for_one_cell(self):
        print()
        for file in os.listdir(path=cell_input_path):
            input_stream = open(cell_input_path + '\\' + file, 'r')
            n, m, unused = game.read_table_params(input_stream)

            # ввод игрового поля
            table = game.read_table(n, m, input_stream)

            next_table = [[cell.Empty(i, j) for j in range(5)] for i in range(5)]

            for i in range(1, n + 1):
                for j in range(1, m + 1):
                    table[i][j].push(table, next_table)

            answer = 'n'
            nearby_same = 0
            fish = 0
            shrimp = 0
            die = False

            for i in range(5):
                for j in range(5):
                    if i == 2 and j == 2:
                        pass
                    else:
                        if table[2][2].cell_type == table[i][j].cell_type:
                            nearby_same += 1
                        if table[i][j].cell_type == 'f':
                            fish += 1
                        if table[i][j].cell_type == 's':
                            shrimp += 1

            if table[2][2].cell_type == 'f' or table[2][2].cell_type == 's':
                if nearby_same < 2 or nearby_same >= 4:
                    die = True
                else:
                    answer = table[2][2].cell_type

            if table[2][2].cell_type == 'r':
                answer = 'r'
            if table[2][2].cell_type == 'n':
                if fish == 3:
                    answer = 'f'
                elif shrimp == 3:
                    answer = 's'
            if die:
                answer = 'n'

            self.assertEqual(answer, next_table[2][2].cell_type, '\n' + file + " - MISMATCH!")
            print('\n', file, "- OK!", end='')
            input_stream.close()
        print()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestsForCells)
    unittest.TextTestRunner(verbosity=2).run(suite)
